from odoo import models, fields, api


from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
import requests
import json



class DriverRouteWizard(models.TransientModel):
    _name = 'driver.route.wizard'
    _description = 'Driver Picking Route Wizard'

    route_type = fields.Selection([
            ('new', 'New'),
            ('existing', 'Existing'),
        ],default='new')
    route_name = fields.Char('Route Name')
    driver_route_id = fields.Many2one('driver.picking.route', string="Driver Route")

    @api.multi
    def attach_pickings(self):
        try:
            if self.route_type == 'existing':
                self.ensure_one()
                picking_active_ids = self.env.context.get('active_ids')
                active_picking_ids = self.env['stock.picking'].browse(picking_active_ids)
                driver_route_ids = self.env['driver.picking.route'].search([])
                for active_pick in active_picking_ids:
                    if active_pick.state != 'assigned' or not active_pick.transporter_route_id:
                        raise ValidationError(_('%s picking is not in "Ready" state or Transporter Route is not yet created for this picking.' %(active_pick.name)))
                
                for picking in active_picking_ids:
                    for driver in driver_route_ids:
                        for data in driver.picking_ids.ids:
                            if picking.ids == [data]:
                                raise ValidationError(_('%s picking already exists in another Driver Picking Route.' %(picking.name)))
                        
                new_pickings = []
                existing_pickings = self.driver_route_id.picking_ids
                for picking_data in active_picking_ids:
                    if picking_data.id not in self.driver_route_id.picking_ids.ids:
                        new_pickings.append(picking_data.id)

                # if((len(new_pickings) + len(existing_pickings)) > 23):
                #     raise ValidationError(_("You cannot add more than 23 pickings per driver route.\n You can now add upto %s for the selected driver route"%(23 - len(existing_pickings))))
                # else:
                for n_pick in new_pickings:
                    self.driver_route_id.write({'picking_ids':[(4, n_pick)]})

                
                google_api_key = self.env["ir.config_parameter"].sudo().get_param("google.api_key_geocode")
                url = 'https://maps.googleapis.com/maps/api/directions/json?'
                
                params = {
                    'origin': str(self.env.user.company_id.partner_id.partner_latitude) + ',' + str(self.env.user.company_id.partner_id.partner_longitude),
                    'destination': str(self.env.user.company_id.partner_id.partner_latitude) + ',' + str(self.env.user.company_id.partner_id.partner_longitude),
                    'waypoints': '',
                    'mode': 'driving',
                    'key':google_api_key,
                }
                for p_id in self.driver_route_id.picking_ids:
                    params['waypoints'] += str(p_id.partner_id.partner_latitude) + ',' + str(p_id.partner_id.partner_longitude) + '|'
                
                params['waypoints'] =  params['waypoints'][:-1]
                
                response = requests.post(url=url, params=params)
                
                data = json.loads(response.text)
                cnt = 0
                sequence_waypoints = []
                if data.get("routes"):
                    for leg in data.get("routes")[0].get('legs'):
                        if cnt > 0:
                            sequence_waypoints.append({'seq' : cnt,'lat' : round(leg.get('start_location').get('lat'), 2), 'lng' : round(leg.get('start_location').get('lng'), 2) })
                        cnt += 1

                for pc_id in self.driver_route_id.picking_ids:
                    for s_way in sequence_waypoints:
                        if s_way.get('lat') == round(pc_id.partner_id.partner_latitude,2) and s_way.get('lng') == round(pc_id.partner_id.partner_longitude,2):
                            pc_id.driver_route_sequence = s_way.get('seq')
            
            if self.route_type == 'new':
                self.ensure_one()
                picking_active_ids = self.env.context.get('active_ids')
                active_picking_ids = self.env['stock.picking'].browse(picking_active_ids)
                driver_route_ids = self.env['driver.picking.route'].search([])
                
                for active_pick in active_picking_ids:
                    if active_pick.state != 'assigned' or not active_pick.transporter_route_id:
                        raise ValidationError(_('%s picking is not in "Ready" state or Transporter Route is not yet created for this picking.' %(active_pick.name)))
                    
                for driver in driver_route_ids:    
                    for picking in active_picking_ids:
                        for data in driver.picking_ids.ids:
                            if picking.ids == [data]:
                                raise ValidationError(_('%s picking already exists in another Driver Picking Route.' %(picking.name)))
                
                new_pickings = []

                # if(len(active_picking_ids.ids) > 23):
                #     raise ValidationError(_("You cannot add more than 23 pickings per driver route.\n You can now add only %s for the selected driver route"%(23)))

                vals = {
                    'name': self.route_name,
                    'picking_ids': [(6, 0, active_picking_ids.ids)]
                }
                driver_route_id = self.env['driver.picking.route'].create(vals)
                
                google_api_key = self.env["ir.config_parameter"].sudo().get_param("google.api_key_geocode")
                url = 'https://maps.googleapis.com/maps/api/directions/json?'
                
                params = {
                    'origin': str(self.env.user.company_id.partner_id.partner_latitude) + ',' + str(self.env.user.company_id.partner_id.partner_longitude),
                    'destination': str(self.env.user.company_id.partner_id.partner_latitude) + ',' + str(self.env.user.company_id.partner_id.partner_longitude),
                    'waypoints': '',
                    'mode': 'driving',
                    'key':google_api_key,
                }
                for p_id in driver_route_id.picking_ids:
                    params['waypoints'] += str(p_id.partner_id.partner_latitude) + ',' + str(p_id.partner_id.partner_longitude) + '|'
                    
                response = requests.post(url=url, params=params)
                
                data = json.loads(response.text)
                cnt = 0
                sequence_waypoints = []
                for leg in data.get("routes")[0].get('legs'):
                    if cnt > 0:
                        sequence_waypoints.append({'seq' : cnt,'lat' : round(leg.get('start_location').get('lat'), 2), 'lng' : round(leg.get('start_location').get('lng'), 2) })
                    cnt += 1
                
                for pc_id in driver_route_id.picking_ids:
                    for s_way in sequence_waypoints:
                        if s_way.get('lat') == round(pc_id.partner_id.partner_latitude,2) and s_way.get('lng') == round(pc_id.partner_id.partner_longitude,2):
                            pc_id.driver_route_sequence = s_way.get('seq')
        except Exception as e:
            raise ValidationError(_(e))
