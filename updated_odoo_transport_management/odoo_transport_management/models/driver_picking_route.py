from odoo import models, fields, api, _


class DriverPickingRoute(models.Model):
    _name = 'driver.picking.route'
    _inherit = 'mail.thread'
    _description = 'Driver Picking Route'

    name = fields.Char('Name')
    picking_ids = fields.Many2many('stock.picking',string='Pickings')
    state = fields.Selection([('draft', 'DRAFT'), ('confirmed', 'CONFIRMED'), ('inprogress', 'IN PROGRESS'), ('finished', 'FINISHED'), ('cancelled', 'CANCELLED')], string='Status',default='draft')

    
    def btn_confirm(self):
        for rec in self:
            rec.state = 'confirmed'

    def btn_draft(self):
        for rec in self:
            rec.state = 'draft'

    def btn_cancel(self):
        for rec in self:
            rec.state = 'cancelled'

    def btn_start(self):
        for rec in self:
            rec.state = 'inprogress'

    @api.multi
    def pre_print_report(self, data):
        data['form'].update({'picking_ids': self.picking_ids})
        return data
    
#     @api.multi
#     def create(self,vals):
#         res = super(DriverPickingRoute,self).create(vals)
#         for driver in res:
#             for picking in driver.picking_ids:
#                 picking.driver_route_state = driver.state
#         return res
    
    @api.multi
    def write(self,vals):
        res = super(DriverPickingRoute,self).write(vals)
        if 'state' in vals:
            for driver in self:
                for picking in driver.picking_ids:
                    picking.driver_route_state = driver.state
        return res

    def btn_finish(self):
        for rec in self:
            rec.state = 'finished'
        data = {'pick_id':self.id,'custom_report':True}
        return self.env.ref('stock.action_report_picking').report_action(self,data=data)


class ReportPickingOperation(models.AbstractModel):
    _name = 'report.ehcs_custom_reports.picking_operation_report'

    @api.model
    def _get_report_values(self, docids, data):
        custom_report = data.get('custom_report')
        picking_data_ids = self.env['stock.picking']
        if custom_report:
            picks_id = data.get('pick_id')
            route_ids = self.env['driver.picking.route'].search([('id', '=', picks_id)])
            picking_data_ids |= route_ids.picking_ids
            return {
                'doc_ids': docids,
                'doc_model': 'stock.picking',
                'data': data,
                'docs': picking_data_ids,
            }
        else:
            for doc_id in docids:
                route_ids = self.env['stock.picking'].browse(doc_id)
                picking_data_ids |= route_ids
            return {
                'doc_ids': None,
                'doc_model': 'stock.picking',
                'data': data,
                'docs': picking_data_ids,
            }


class StockPickingInherit(models.Model):
    _inherit = 'stock.picking'

    driver_note = fields.Text('Driver Note')