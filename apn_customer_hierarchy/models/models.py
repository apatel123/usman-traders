# -*- coding: utf-8 -*-

from odoo import models, fields, api

class apn_customer_hierarchy(models.Model):
    _inherit = 'res.partner'

    territory = fields.Char(string="Territory")
    location = fields.Char(string="Location")
    responsible = fields.Many2one('apn.responsible',string="Responsible")
    order_book = fields.Many2one('apn.order.book', string="Order Booker")
    Delivery_man = fields.Many2one('apn.delivery.man', string="Delivery Man")

class saleorderext(models.Model):
     _inherit = 'sale.order'

     territory = fields.Char(string="Territory")
     location = fields.Char(string="Location")
     order_book = fields.Many2one('apn.order.book',string="Order Booker")
     Delivery_man = fields.Many2one('apn.delivery.man',string="Delivery Man")