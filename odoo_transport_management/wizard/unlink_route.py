





from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
import requests
import json


class UnlinkDriverRoute(models.TransientModel):
    _name = 'unlink.route.wizard'
    _description = 'Unlink Driver Route Wizard'

    @api.multi
    def remove_pickings(self):
        picking_active_ids = self.env.context.get('active_ids')
        active_picking_ids = self.env['stock.picking'].browse(picking_active_ids)
        driver_route_ids = self.env['driver.picking.route'].search([])

        try:
            for picking in active_picking_ids:
                for driver in driver_route_ids:
                    if picking.id in driver.picking_ids.ids:
                        driver.write({'picking_ids': [(3, picking.id)]})
                        picking.driver_route_sequence = 0
                        
                        if (len(driver.picking_ids) <= 23):
                            google_api_key = self.env["ir.config_parameter"].sudo().get_param("google.api_key_geocode")
                            url = 'https://maps.googleapis.com/maps/api/directions/json?'
                            
                            params = {
                                'origin': str(self.env.user.company_id.partner_id.partner_latitude) + ',' + str(self.env.user.company_id.partner_id.partner_longitude),
                                'destination': str(self.env.user.company_id.partner_id.partner_latitude) + ',' + str(self.env.user.company_id.partner_id.partner_longitude),
                                'waypoints': '',
                                'mode': 'driving',
                                'key':google_api_key,
                            }
                            for p_id in driver.picking_ids:
                                params['waypoints'] += str(p_id.partner_id.partner_latitude) + ',' + str(p_id.partner_id.partner_longitude) + '|'
                            
                            params['waypoints'] =  params['waypoints'][:-1]
                            
                            response = requests.post(url=url, params=params)
                            
                            data = json.loads(response.text)
                            cnt = 0
                            sequence_waypoints = []
                            for leg in data.get("routes")[0].get('legs'):
                                if cnt > 0:
                                    sequence_waypoints.append({'seq' : cnt,'lat' : round(leg.get('start_location').get('lat'), 2), 'lng' : round(leg.get('start_location').get('lng'), 2) })
                                cnt += 1
                            
                            for pc_id in driver.picking_ids:
                                for s_way in sequence_waypoints:
                                    if s_way.get('lat') == round(pc_id.partner_id.partner_latitude,2) and s_way.get('lng') == round(pc_id.partner_id.partner_longitude,2):
                                        pc_id.driver_route_sequence = s_way.get('seq')
        except Exception as e:
            raise ValidationError(_(e))




