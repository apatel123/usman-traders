# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class DriverRouteWizard(models.TransientModel):
    _name = 'driver.route.wizard'
    _description = 'Driver Picking Route Wizard'

    route_type = fields.Selection([
            ('new', 'New'),
            ('existing', 'Existing'),
        ],)

    # attrs = "{'invisible': [('internal_type','=','liquidity')]}"

    @api.multi
    def attach_pickings(self):
        # use active_ids to add picking line to the selected Route
        self.ensure_one()
        picking_ids = self.env.context.get('active_ids')
        return self.env['stock.picking'].browse(picking_ids).write({'batch_id': self.batch_id.id})


    # @api.onchange('route_type')
    # def onchange_route_type(self):
    #     if self.route_type == 'new':

