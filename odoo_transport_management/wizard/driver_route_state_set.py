from odoo import models, fields, api

class DriverRouteWizard(models.TransientModel):
    _name = 'driver.route.state.set'
    
    
    @api.multi
    def set_driver_route_state(self):
        self.ensure_one()
        picking_active_ids = self.env.context.get('active_ids')
        active_picking_ids = self.env['stock.picking'].browse(picking_active_ids)

        for picking_id in active_picking_ids:
            driver_route = self.env['driver.picking.route'].sudo().search([('picking_ids','in',[picking_id.id])],limit=1)
            if driver_route:
                picking_id.driver_route_state = driver_route.state
    
    